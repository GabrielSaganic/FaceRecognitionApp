﻿using OpenCvSharp;
using OpenCvSharp.Face;
using Plugin.Media;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FaceRecognitionApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RecognitionPage : ContentPage
    {
        private CascadeClassifier cascadeClassifier;
        private EigenFaceRecognizer faceRecognizer;
        private OpenCvSharp.Size faceSize = new OpenCvSharp.Size() { Height = 150, Width = 150 };
        private SortedDictionary<int, String> indexToLabelName = new SortedDictionary<int, string>();
        private int numImg;

        public RecognitionPage()
        {
            InitializeComponent();

            //ucitavamo novi klasifikator koji koristimo za prepoznavanje prednje strane lica
            cascadeClassifier = new CascadeClassifier("haarcascade_frontalface_alt2.xml");
            //kreiramo EigenFaceRecognizer algoritam  s maksimalno 15000 komponenata na slici s najvecim dopustenim odstupanjem 10000 
            faceRecognizer = EigenFaceRecognizer.Create(15000, 10000);

            //ucitvamo put do datoke gdje se sprema istrenirani model
            var recognizerPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "faceRecognizer.xml");
            if (File.Exists(recognizerPath))
                faceRecognizer.Read(recognizerPath);

            var folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "recognize");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            //citamo broj slika u folderu
            numImg = Directory.GetFiles(folder).ToList<string>().Count;

            //putanja do datoteke u kojoj su zapisana imena i brojcane oznake
            var labelsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "labelPaths.txt");

            //ucitavanje imena i oznaka
            var lines = File.ReadLines(labelsPath);
            foreach (var line in lines)
            {
                var parts = line.Split(' ');
                indexToLabelName.Add(Int32.Parse(parts[0]), parts[1]);
            }
        }

        private async void CameraButton_Clicked(object sender, EventArgs e)
        {
            //put do foldera gdje se spremaju slike
            string imagePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "recognize");
            //generiramo ime slike
            string imageName = "test " + numImg++ + ".jpg";
            //otvaramo kameru te novu sliku pospremamo u recognize direktorij
            Plugin.Media.Abstractions.MediaFile photo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions()
            {
                Directory = "recognize",
                Name = imageName
            });
            if (photo == null)
                return;

            //ucitavamo matricu sa bojama za prikaz
            Mat result = Mat.FromStream(photo.GetStream(), ImreadModes.Color);
            //ucitavamo matricu bez boja za prepoznavanje(limitacija egien faces recognizer)
            var photoCv = Mat.FromStream(photo.GetStream(), ImreadModes.Grayscale);

            //oslobadamo memoriju
            photo.Dispose();

            //ucitavanje okvira lica u varijablu _faces
            Rect[] _faces = cascadeClassifier.DetectMultiScale(
                               photoCv, 1.08, 3, HaarDetectionType.ScaleImage);

            foreach (Rect face in _faces)
            {
                int label = -1;
                double confidence = 0;
                //resizamo sliku lica na 150x150 pixela
                var resizedFace = photoCv[face].Clone().Resize(faceSize);
                faceRecognizer.Predict(resizedFace, out label, out confidence);
                //stavljamo okvir, ime i postotak sigurnosti da je prepoznao osobu
                confidence = Math.Truncate(((10000 - confidence) / 10000) * 100);
                if (label != -1)
                {
                    var textPoint = new OpenCvSharp.Point
                    {
                        X = (int)(face.X + face.Width * 0.5 - indexToLabelName[label].Length * 30 / 2),
                        Y = (int)(face.Y - 10)
                    };

                    Cv2.PutText(result, indexToLabelName[label] + " " + confidence.ToString() + "%", textPoint, HersheyFonts.HersheyPlain, 2, new Scalar(255, 255, 255), 2);
                    Cv2.Rectangle(result, face, new Scalar(255, 64, 64), 2);
                }
                //ukoliko osoba nije prepoznata postavlja ime na unknown
                else
                {
                    var textPoint = new OpenCvSharp.Point
                    {
                        X = (int)(face.X + face.Width * 0.5 - "unknown".Length * 30 / 2),
                        Y = (int)(face.Y - 10)
                    };

                    Cv2.PutText(result, "unknown", textPoint, HersheyFonts.HersheyPlain, 2, new Scalar(255, 255, 255), 2);
                    Cv2.Rectangle(result, face, new Scalar(255, 64, 64), 2);
                }
            }

            //postavljanje slike kao pozadinu sucelja
            PhotoImage.Source = ImageSource.FromStream(() => { return result.ToMemoryStream(); });
            //zapisivanje slike na prethodno definiranu putanju
            Cv2.ImWrite(Path.Combine(imagePath, imageName), result);
            
        }

        //pokusaj normalizacije face (neuspjesan) :( 
        private async void Test(object sender, EventArgs e)
        {
            //citanje puta do test datoteke
            string imagePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "test");
            if (!Directory.Exists(imagePath))
                Directory.CreateDirectory(imagePath);

            //otvaranje kamere te pospremanje slike u test direktorij
            Plugin.Media.Abstractions.MediaFile photo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions()
            {
                Directory = "test",
                Name = "etst.jpg"
            });
            if (photo == null)
                return;

            //ucitavanje matrice u boji
            var photoCv = Mat.FromStream(photo.GetStream(), ImreadModes.Color);

            //cistimo memoriju 
            photo.Dispose();

            Rect[] _faces = cascadeClassifier.DetectMultiScale(
                               photoCv, 1.08, 3, HaarDetectionType.ScaleImage);

            //za svaku sliku pokusavamo poravnati facu
            foreach (Rect face in _faces)
            {
                Mat rotatedFace = Normalizer.NormalizeFace(photoCv, photoCv[face].Clone());
                PhotoImage.Source = ImageSource.FromStream(() => { return rotatedFace.ToMemoryStream(); });
            }
        }
    }
}

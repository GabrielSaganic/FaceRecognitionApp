﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace FaceRecognitionApp
{
    class Normalizer
    {
        public static CascadeClassifier eyeDetector = new CascadeClassifier("haarcascade_eye.xml");
        public static CascadeClassifier leftEyeDetector = new CascadeClassifier("haarcascade_lefteye_2splits.xml");
        public static CascadeClassifier rightEyeDetector = new CascadeClassifier("haarcascade_righteye_2splits.xml");

        public static Mat NormalizeFace(Mat wholePhoto, Mat face)
        {
            //postavljamo zeljene pozicije za oci, u omjerima na visinu i sirinu
            double desiredLeftEyeY = 0.35;
            double desiredLeftEyeX = 0.35;
            double desiredRightEyeY = 0.35;
            double desiredRightEyeX = 1 - desiredLeftEyeX;
            Mat result = new Mat();
            //postavljamo zeljenu velicinu slike na dimenzije 150x150
            Size dsize = new Size(150, 150);

            //okviri u koje se spremaju pozicije ociju leftEye i rightEye
            Rect[] leftEye = leftEyeDetector.DetectMultiScale(
                               face, 1.08, 3, HaarDetectionType.ScaleImage);
            Rect[] rightEye = rightEyeDetector.DetectMultiScale(
                               face, 1.08, 3, HaarDetectionType.ScaleImage);
            //if (eyes.Length == 2) //relikvija prethodnog pokusaja
            {
                //racunanje koordinata ljevog i desnog oka
                Point leftEyeCenter = new Point(leftEye[0].X + leftEye[0].Width / 2, leftEye[0].Y + leftEye[0].Height / 2);
                Point rightEyeCenter = new Point(rightEye[1].X + rightEye[1].Width / 2, rightEye[1].Y + rightEye[1].Height / 2);

                //Y i X razlike izmedu ociju 
                int dY = rightEyeCenter.Y - leftEyeCenter.Y;
                int dX = rightEyeCenter.X - leftEyeCenter.X;

                //racunanje kuta, te stvarne udaljenosti izmedu ociju i zeljene udaljenosti
                double angle = Math.Atan2(dY, dX) * (float)(180 / Math.PI) - 180;
                double dist = Math.Sqrt(Math.Pow(dX, 2) + Math.Pow(dY, 2));
                double desiredDist = (desiredRightEyeX - desiredLeftEyeY) * 150;

                //pronalazimo omjer izmedu zeljene i stvarne velicine 
                double scale = desiredDist / dist;

                //trazimo centar izmedu ociju, oko kojeg cemo rotirati sliku
                Point eyeCenter = new Point((leftEyeCenter.X + rightEyeCenter.X) / 2, (leftEyeCenter.Y + rightEyeCenter.Y) / 2);

                //matrica koja nam sluzi za rotaciju slike
                Mat m = Cv2.GetRotationMatrix2D(eyeCenter, angle, scale);

                double tX = 150 * desiredLeftEyeX, tY = 150 * desiredLeftEyeY;
                
                //m[0, 2] += (tX - eyeCenter.X);
                //m[1, 2] += (tY - eyeCenter.Y);

                //funkcija koja radi rotaciju slike
                Cv2.WarpAffine(face, result, m, dsize, InterpolationFlags.Cubic);
            }
            return result;
        }
    }
}

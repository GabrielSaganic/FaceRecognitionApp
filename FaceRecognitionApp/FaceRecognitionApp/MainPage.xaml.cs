﻿using OpenCvSharp;
using OpenCvSharp.Face;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Media;

namespace FaceRecognitionApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        //naredbe za navigaciju na druge stranice u sucelju
        private async void GoToTraining(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new TrainingPage());
        }

        private async void GoToRecognize(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new RecognitionPage());
        }
    }
}

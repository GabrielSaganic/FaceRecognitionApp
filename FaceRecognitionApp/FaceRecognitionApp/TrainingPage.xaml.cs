﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Media;
using System.IO;
using OpenCvSharp;
using OpenCvSharp.Face;

namespace FaceRecognitionApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrainingPage : ContentPage
    {
        private CascadeClassifier cascadeClassifier;
        private EigenFaceRecognizer faceRecognizer;
        private OpenCvSharp.Size faceSize = new OpenCvSharp.Size() { Height = 150, Width = 150 };

        private List<Mat> faces = new List<Mat>();
        private List<String> labels = new List<string>();
        private SortedDictionary<String, int> nameToLabelIndex = new SortedDictionary<string, int>();
        private SortedDictionary<String, int> numberOfImagesPerPerson = new SortedDictionary<string, int>();

        public TrainingPage()
        {
            InitializeComponent();
            //povezujemo metodu na dogadaj pri izlaska iz stranice
            this.Disappearing += TrainingPage_Disappearing;
            //ucitavamo novi klasifikator koji koristimo za prepoznavanje prednje strane lica
            cascadeClassifier = new CascadeClassifier("haarcascade_frontalface_alt2.xml");
            //kreiramo EigenFaceRecognizer algoritam  s maksimalno 15000 komponenata na slici s najvecim dopustenim odstupanjem 1000 
            faceRecognizer = EigenFaceRecognizer.Create(15000, 10000);
            //ucitvamo put do datoke gdje se sprema istrenirani model
            var recognizerPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "faceRecognizer.xml");
            //ako datoteka postoji citamo iz datoteke
            if (File.Exists(recognizerPath))
                faceRecognizer.Read(recognizerPath);
            //spremamo put do datoteke training u varijablu folder
            var folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "training");
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);
            //ucitavamo sve slike iz datoteke training
            var allTrainingImages = Directory.GetFiles(folder);
            //prolazimo kroz svaku sliku
            foreach (var image in allTrainingImages)
            {
                //ucitavamo matricu sa slike, u crno-bijelom nacinu
                var cvImage = new Mat(image, ImreadModes.Grayscale);
                faces.Add(cvImage);
                
                //povecavamo zadnji broj slike osobe za 1
                var lastIndex = image.LastIndexOf(@"\") + 1;
                //uklanjamo nastavak i broj sa slike da ih mozemo sve pridruziti istoj osobi
                var label = image.Remove(0, lastIndex).Replace(".jpg", "").Replace("0", "").Replace("1", "").Replace("2", "").Replace("3", "").Replace("4", "").Replace("5", "").Replace("6", "").Replace("7", "").Replace("8", "").Replace("9", "");
                //ako ime jos ne postoji, pridruzujemo mu novi broj preko kojeg ce se ucit
                if (!nameToLabelIndex.ContainsKey(label))
                {
                    nameToLabelIndex.Add(label, nameToLabelIndex.Count);
                }
                //ako ne postoji, dodajemo ime i postavljamo broj slika na 1
                if (!numberOfImagesPerPerson.ContainsKey(label))
                    numberOfImagesPerPerson.Add(label, 1);
                //ako postoji, povecavamo broj slike za 1
                else
                    numberOfImagesPerPerson[label] += 1;
                labels.Add(label);
            }
        }

        //prilikom izlaska iz stranice spremamo model
        private void TrainingPage_Disappearing(object sender, EventArgs e)
        {
            var recognizerPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "faceRecognizer.xml");
            faceRecognizer.Save(recognizerPath);
            saveLabels();
        }

        //spremamo labels u obliku "ime index"
        private void saveLabels()
        {
            var labelsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "labelPaths.txt");
            List<string> lines = new List<string>();
            foreach (var pair in nameToLabelIndex)
            {
                lines.Add(pair.Value.ToString() + ' ' + pair.Key);
            }
            File.WriteAllLines(labelsPath, lines);
        }

        private async void CameraButton_Clicked(object sender, EventArgs e)
        {
            //stvaramo u varijablu name , ime osobe
            String name = TrainingName.Text;

            //ako ne postoji, dodajemo ime i povecavamo broj slike za 1
            if (!numberOfImagesPerPerson.ContainsKey(name))
                numberOfImagesPerPerson.Add(name, 1);
            //ako postoji, povecavamo broj slike za 1
            else
                numberOfImagesPerPerson[name] += 1;
            //otvara kameru i sprema u training datoteku sliku
            Plugin.Media.Abstractions.MediaFile photo = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions()
            {
                Directory = "training",
                Name = name + numberOfImagesPerPerson[name] + ".jpg"
            });
            //sprema put do training foldera
            string imagePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "training");
            //i
            string imageName = name + numberOfImagesPerPerson[name] + ".jpg";
            if (photo == null)
                return;
            //ucitavamo matricu sa slike, u boji
            var photoCv = Mat.FromStream(photo.GetStream(), ImreadModes.Color);
            //oslobadamo memoriju
            photo.Dispose();

            //definiramo varijablu _faces u kojoj se zapisu okviri pronadenih lica :)
            Rect[] _faces = cascadeClassifier.DetectMultiScale(
                               photoCv, 1.08, 3, HaarDetectionType.ScaleImage);

            //za svako lice resizeamo sliku te dodajemo tekst imena i okvir
            foreach (Rect face in _faces)
            {
                var justTheFace = photoCv[face].Clone().Resize(faceSize);
                Cv2.ImWrite(Path.Combine(imagePath, imageName), justTheFace);

                // Mat normalizedFace = Normalizer.Normalize(photoCv, justTheFace);

                var textPoint = new OpenCvSharp.Point
                {
                    X = (int)(face.X + face.Width * 0.5 - name.Length * 30 / 2),
                    Y = (int)(face.Y - 10)
                };
                Cv2.PutText(photoCv, name, textPoint, HersheyFonts.HersheyPlain, 3, new Scalar(255, 255, 255), 2);
                Cv2.Rectangle(photoCv, face, new Scalar(255, 64, 64), 2);
                faces.Add(photoCv[face].Clone().Resize(faceSize));
                if (!nameToLabelIndex.ContainsKey(name))
                {
                    nameToLabelIndex.Add(name, nameToLabelIndex.Count);
                }
                labels.Add(name);

            }

            //dodajemo u pozadini sliku 
            PhotoImage.Source = ImageSource.FromStream(() => { return photoCv.ToMemoryStream(); });
            //try { photoCv.Dispose(); }
            //catch { await DisplayAlert("Something wierd happened with OpenCV", "No one knows what is going on", "god help us all"); }

            //pospremamo u integer _labels broj osobe koju zelimo trenirat 
            List<int> _labels = new List<int>();
            foreach (var label in labels)
            {
                _labels.Add(nameToLabelIndex[label]);
            }
            //te saljemo sliku i broj osobe na treniranje face
            faceRecognizer.Train(faces, _labels);
        }


        //ponovno treniranje po volji
        private void ForceRetrain(object sender, EventArgs e)
        {
            List<int> _labels = new List<int>();
            foreach (var label in labels)
            {
                _labels.Add(nameToLabelIndex[label]);
            }

            faceRecognizer.Train(faces, _labels);
        }
    }
}